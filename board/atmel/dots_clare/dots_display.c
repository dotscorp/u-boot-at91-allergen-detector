
/***************************************************
* Module name: display.c
*
* Copyright 2018 DOTS Technology Corp as an unpublished work. 
* All Rights Reserved.
*
*  The information contained herein is confidential 
* property of DOTS Technology Corp. The user, copying, 
* transfer or disclosure of such information is prohibited 
* except by express written agreement with DOTS Technology Corp.
*
* First written on 03/05/2018 by E. Robertson
*
* Module Description:
* This file contains display specific functions.
*
*
***************************************************/

/*  Include section
* Add all #includes here
*
**************************************************/
//#include <stdint.h>
#include <linux/delay.h>
#include <asm/io.h>
#include <asm/arch/at91_common.h>
#include <asm/arch/atmel_pio4.h>
#include <common.h>
//#include <dm.h>
#include <errno.h>
#include <spi.h>

#include "display.h"
#include "display_data.h"

/*  Private Defines section
* Add all #defines here
*
***************************************************/

#define DISPLAY_OFF									(0xAE)
#define DISPLAY_ON 									(0xAF)
#define SET_COLUMN_ADDRESS 					(0x15)
#define SET_ROW_ADDRESS 						(0x75)
#define SET_CONTRAST								(0x81)
#define SET_REMAP										(0xA0)
#define SET_DISPLAY_START_LINE			(0xA1)
#define SET_DISPLAY_OFFSET					(0xA2)
#define SET_VERTICAL_SCROLL					(0xA3)
#define SET_MODE_NORMAL							(0xA4)
#define SET_MODE_ALL_ON							(0xA5)
#define SET_MODE_ALL_OFF						(0xA6)
#define SET_MODE_INVERSE_DISPLAY		(0xA7)
#define SET_MUX_RATIO								(0xA8)
#define REGULATOR_SELECT						(0xAB)
#define SET_PHASE_LENGTH						(0xB1)
#define NO_OPERATION								(0xB2)
#define SET_OSCILLATOR_FREQ					(0xB3)
#define GPIO_INPUT									(0xB5)
#define SET_SECOND_PRECHARGE_PERIOD	(0xB6)
#define SET_GRAY_SCALE_TABLE				(0xB8)
#define LINEAR_LUT									(0xB9)
#define SET_PRECHARGE_VOLTAGE				(0xBC)
#define SET_VCOMH										(0xBE)
#define FUNCTION_B									(0xD5)
#define SET_COMMAND_LOCK						(0xFD)

/*  External Variables Section
* Add external variables used by this module here.
*
***************************************************/


/*  Private Variables Section
* Add private variables used by this module here.
*
***************************************************/

const tImage welcome = { image_data_welcome, 0, 0, 80, 64};

static struct spi_slave *slave = NULL;
static struct udevice *dev = NULL;


//static uint8_t screen_Buffer[FULL_DISPLAY_BYTES];


/*  Private Function Prototypes
*
***************************************************/
static void setRowAddress(char rowStart, char rowEnd);
static void setColAddress(char colStart, char colEnd);
static void init_SSD1327(void);
static void display_Write_Data(const uint8_t *data, uint16_t size);
static void display_Write_Reg_Command(uint8_t data);
//static void add_Image(const tImage *img1);
static void clear_UI_Reset(void);
static void set_UI_Reset(void);
static void display_Rail_On(void);
static void display_Rail_Off(void);
static void clear_UI_DC(void);
static void set_UI_DC(void);

/*  Function Prototype Section
* Add prototypes for all functions called by this
* module, with the exception of runtime routines.
*
***************************************************/



static void display_interface_write(const uint8_t *data, uint16_t length)
{
	int ret;
	ret = spi_xfer(slave, length*8, data, NULL,
		       SPI_XFER_BEGIN | SPI_XFER_END);

	if (ret)
		printf("spi_xfer error!\n");
}


static void display_init_bus(void)
{
	int ret = 0;

	ret = spi_get_bus_and_cs(0, 0, 1000000, 3, "spi_generic_drv",
				 "generic_0:0", &dev, &slave);

	if (ret)
	{
		printf("SPI_GET_BUS returned %d!\n",ret);
		return;
	}	
}


static void display_claim_bus(void)
{
	int ret;

	ret = spi_claim_bus(slave);
	if (ret)
	{
		printf("SPI_CLAIM_BUS returned %d!\n",ret);
		return;
	}
}



static void display_release_bus(void)
{
	if(slave)
	{
		spi_release_bus(slave);
	}
}


/*********display_Write_Reg_Command()*********************
*	Description: 
*			Sends register command to display IC.
*
*	Inputs: None.
*	
*	Outputs: None.
*****************************************************/
static void display_Write_Reg_Command(uint8_t data) {
	display_interface_write(&data, 1);
}

/*********display_Write_Data()*********************
*	Description: 
*			Sends data to display IC.
*
*	Inputs: None.
*	
*	Outputs: None.
*****************************************************/
static void display_Write_Data(const uint8_t *data, uint16_t size) {
	display_interface_write(data, size);
}




/*********display_Power_On()*********************

*	Description: 
*			Power on and initialize display.
*
*	Inputs: None.
*	
*	Outputs: None.
*****************************************************/
// New version for UBOOT
uint8_t display_Power_On(uint8_t ignored) {

	display_init_bus();
	display_claim_bus();

	// Pulse RST low for at least 100 us
	clear_UI_Reset();
	udelay(200);
	set_UI_Reset();
	display_Rail_On();
	udelay(10000);	// Stabilize

	init_SSD1327();

	display_release_bus();
	return 0;
}

/*********init_SSD1327()*********************
*	Description: 
*			Initialize display IC.
*
*	Inputs: None.
*	
*	Outputs: None.
*****************************************************/
static void init_SSD1327(void) {	
	clear_UI_DC();
	
	display_Write_Reg_Command(DISPLAY_OFF);	
	display_Write_Reg_Command(SET_COLUMN_ADDRESS);
	display_Write_Reg_Command(0x00);
	display_Write_Reg_Command(0x4F);
	display_Write_Reg_Command(SET_ROW_ADDRESS);
	display_Write_Reg_Command(0x3F);
	display_Write_Reg_Command(0x00);
	display_Write_Reg_Command(SET_CONTRAST);
	display_Write_Reg_Command(0xB0);
	display_Write_Reg_Command(SET_REMAP);
	display_Write_Reg_Command(0x50);
	display_Write_Reg_Command(SET_DISPLAY_START_LINE);
	display_Write_Reg_Command(0x00);
	display_Write_Reg_Command(SET_DISPLAY_OFFSET);
	display_Write_Reg_Command(0x00);
	display_Write_Reg_Command(SET_VERTICAL_SCROLL);
	display_Write_Reg_Command(0x00);
	display_Write_Reg_Command(0x40);
	display_Write_Reg_Command(SET_MODE_NORMAL);	
 	display_Write_Reg_Command(SET_MUX_RATIO);
	display_Write_Reg_Command(0x3F);
	display_Write_Reg_Command(REGULATOR_SELECT);
	display_Write_Reg_Command(0x01);
	display_Write_Reg_Command(0xAD);
	display_Write_Reg_Command(0x8E);
	display_Write_Reg_Command(SET_PHASE_LENGTH);
	display_Write_Reg_Command(0x82);
	display_Write_Reg_Command(SET_OSCILLATOR_FREQ);
	display_Write_Reg_Command(0xA0);
	display_Write_Reg_Command(SET_SECOND_PRECHARGE_PERIOD);
	display_Write_Reg_Command(0x04);
	display_Write_Reg_Command(LINEAR_LUT);
	display_Write_Reg_Command(SET_PRECHARGE_VOLTAGE);
	display_Write_Reg_Command(0x04);
	display_Write_Reg_Command(0xBD);
	display_Write_Reg_Command(0x01);
	display_Write_Reg_Command(SET_VCOMH);
	display_Write_Reg_Command(0x05);
	display_Write_Reg_Command(DISPLAY_ON);
	
	set_UI_DC();

//	set_Display_Blank();
}

/*********setColAddress()*********************
*	Description: 
*			Set start and end column address.
*
*	Inputs: None.
*	
*	Outputs: None.
*****************************************************/
static void setColAddress(char colStart, char colEnd) {
	clear_UI_DC();
	colStart = colStart + (0x30);
	colEnd = colEnd + (0x30);
	display_Write_Reg_Command(SET_COLUMN_ADDRESS);
	display_Write_Reg_Command(colStart);
	display_Write_Reg_Command(colEnd);
}

/*********setRowAddress()*********************
*	Description: 
*			Set start and end row address.
*
*	Inputs: None.
*	
*	Outputs: None.
*****************************************************/
static void setRowAddress(char rowStart, char rowEnd) {
	clear_UI_DC();
	display_Write_Reg_Command(SET_ROW_ADDRESS);
	display_Write_Reg_Command(rowStart);
	display_Write_Reg_Command(rowEnd);
}


/*********set_Display_Welcome()*********************
*	Description: 
*			Send the welcome screen to the display.
*
*	Inputs: None.
*	
*	Outputs: None.
*****************************************************/
void set_Display_Welcome(void) {
	display_claim_bus();

	setColAddress(0x00,0x4F);
	setRowAddress(0x00,0x3F);
	
	set_UI_DC();
	display_Write_Data(&welcome.data[0],FULL_DISPLAY_BYTES);

	display_release_bus();
}




static void clear_UI_Reset(void) {
	atmel_pio4_set_pio_output(AT91_PIO_PORTA, 13, 0);
}
static void set_UI_Reset(void) {
	atmel_pio4_set_pio_output(AT91_PIO_PORTA, 13, 1);
}
static void display_Rail_On(void) {
	atmel_pio4_set_pio_output(AT91_PIO_PORTA, 25, 0);
}
static void display_Rail_Off(void) {
	atmel_pio4_set_pio_output(AT91_PIO_PORTA, 25, 1);
}
static void clear_UI_DC(void) {
	atmel_pio4_set_pio_output(AT91_PIO_PORTA, 22, 0);
}
static void set_UI_DC(void) {
	atmel_pio4_set_pio_output(AT91_PIO_PORTA, 22, 1);
}
