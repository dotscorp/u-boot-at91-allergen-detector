/***************************************************
* Module name: display.h
*
* Copyright 2018 DOTS Technology Corp as an unpublished work. 
* All Rights Reserved.
*
*  The information contained herein is confidential 
* property of DOTS Technology Corp. The user, copying, 
* transfer or disclosure of such information is prohibited 
* except by express written agreement with DOTS Technology Corp.
*
* First written on 03/05/2018 by E. Robertson
*
* Module Description:
* This module is the header file for display.c.
* Public functions, defines and variables are placed here.
*
**************0***********************************/

/*  Defines section
* Add all #defines here
*
***************************************************/
#define P1_DISPLAY_ORIENTATION	1
//#define OPPOSITE_ORIENTATION	1

enum Allergens{PEANUT, TREENUT, MILK, SHELLFISH, FISH, EGG, SOY, GLUTEN, TREENUTPEANUT};
enum Errors{UI_TILT, UI_HOT, UI_COLD, UI_LOW_BATT, UI_DEAD_BATT, 
						UI_POD_REMOVED, UI_HOM_STALL, UI_GEN_ERROR};
enum pod_Errors{UI_USED, UI_NOT_CLARE, UI_CLEAN, UI_EXP, UI_RECALL};
enum Test_Steps{EXTRACTING, FILTERING, BINDING, ANALYZING};
enum Epi_Steps{EPI_QUESTION, EPI_NO, EPI_YES, EPI_PRESENT};
enum Results{ALLERGEN_DETECTED, ALLERGEN_NOT_DETECTED};
enum Display_Init_States{DISPLAY_PIN_INIT, DISPLAY_RESET_DELAY, DISPLAY_TURN_ON_DELAY, DISPLAY_VCC_STABLE_DELAY, DISPLAY_ON};

	
uint8_t display_Power_On(uint8_t input_Val);
uint8_t display_Power_Off(void);

void set_Display_Blank(void);
void set_Display_Welcome(void);
void set_Display_Test_Ready(uint8_t allergen);
void set_Display_Testing(uint8_t test_step);
void set_Display_Epi(uint8_t epi_step);
void set_Display_Pod_Ready(void);
void set_Display_Result(uint8_t result, uint8_t allergen);
void set_Display_Charging(void);
void display_Animation(void);
void display_Charging_Animation(uint8_t step);
void set_Display_Sampling_Tips(void);
void set_Display_Sys_Update_Required(void);
void set_Display_Sys_Update_Success(void);
void set_Display_BT_Connecting(void);
void set_Display_BT_Success(void);
void set_Display_BT_Fail(void);
void set_Display_Error(uint8_t error_type, uint8_t error_number);
void set_Display_Pod_Error(uint8_t error_type);
uint8_t display_Ready(void);

//mkt demo
void set_Display_Flow_Select(uint8_t flow);
extern uint8_t ui_Allergen;

