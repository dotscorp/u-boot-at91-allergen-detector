/*
 * Copyright (C) 2015 Atmel Corporation
 *		      Wenyou.Yang <wenyou.yang@atmel.com>
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include <atmel_lcd.h>
#include <debug_uart.h>
#include <dm.h>
#include <i2c.h>
#include <nand.h>
#include <version.h>
#include <video.h>
#include <asm/io.h>
#include <dm.h>
#include <errno.h>
#include <spi.h>

#ifdef CONFIG_DM_VIDEO
#include <video_console.h>
#endif
#include <asm/io.h>
#include <asm/arch/at91_common.h>
#include <asm/arch/atmel_pio4.h>
#include <asm/arch/atmel_mpddrc.h>
#include <asm/arch/atmel_sdhci.h>
#include <asm/arch/clk.h>
#include <asm/arch/gpio.h>
#include <asm/arch/sama5d2.h>
#include <asm/arch/sama5d2_smc.h>

#include "dots_display.h"

DECLARE_GLOBAL_DATA_PTR;


// RWS: SPI init
static void board_spi_init(void)
{
	// Map the correct IOSet to SPI
	atmel_pio4_set_a_periph(AT91_PIO_PORTA, 14, 0); 
	atmel_pio4_set_a_periph(AT91_PIO_PORTA, 15, 0); 
	atmel_pio4_set_a_periph(AT91_PIO_PORTA, 16, 0); 

	// GPIOs for RST and C/D
	atmel_pio4_set_gpio(AT91_PIO_PORTA, 13, ATMEL_PIO_DIR_MASK);
	atmel_pio4_set_gpio(AT91_PIO_PORTA, 22, ATMEL_PIO_DIR_MASK);
	// DISP_EN
	atmel_pio4_set_gpio(AT91_PIO_PORTA, 25, ATMEL_PIO_DIR_MASK);
	atmel_pio4_set_pio_output(AT91_PIO_PORTA, 25, 1);

}



static void board_usb_hw_init(void)
{
	atmel_pio4_set_pio_output(AT91_PIO_PORTB, 10, 1);
}

#if defined(CONFIG_DM_VIDEO) && defined(CONFIG_ATMEL_HLCD)
static int video_show_board_logo_info(void)
{
	ulong dram_size;
	int i;
	u32 len = 0;
	char buf[255];
	char *corp = "2017 Microchip Technology Inc.\n";
	char temp[32];
	struct udevice *dev, *con;
	const char *s;
	vidinfo_t logo_info;
	int ret;

	get_microchip_logo_info(&logo_info);

	len += sprintf(&buf[len], "%s\n", U_BOOT_VERSION);
	memcpy(&buf[len], corp, strlen(corp));
	len += strlen(corp);
	len += sprintf(&buf[len], "%s CPU at %s MHz\n", get_cpu_name(),
			strmhz(temp, get_cpu_clk_rate()));

	dram_size = 0;
	for (i = 0; i < CONFIG_NR_DRAM_BANKS; i++)
		dram_size += gd->bd->bi_dram[i].size;

	len += sprintf(&buf[len], "%ld MB SDRAM\n", dram_size >> 20);

	ret = uclass_get_device(UCLASS_VIDEO, 0, &dev);
	if (ret)
		return ret;

	ret = video_bmp_display(dev, logo_info.logo_addr,
				logo_info.logo_x_offset,
				logo_info.logo_y_offset, false);
	if (ret)
		return ret;

	ret = uclass_get_device(UCLASS_VIDEO_CONSOLE, 0, &con);
	if (ret)
		return ret;

	vidconsole_position_cursor(con, 0, logo_info.logo_height);
	for (s = buf, i = 0; i < len; s++, i++)
		vidconsole_put_char(con, *s);

	return 0;
}
#endif

#ifdef CONFIG_BOARD_LATE_INIT
int board_late_init(void)
{
#ifdef CONFIG_DM_VIDEO
	video_show_board_logo_info();
#endif

//	printf("board_late_init\n");
	display_Power_On(0);

	set_Display_Welcome();

	return 0;
}
#endif

#ifdef CONFIG_DEBUG_UART_BOARD_INIT
static void board_uart1_hw_init(void)
{
	atmel_pio4_set_a_periph(AT91_PIO_PORTD, 2, ATMEL_PIO_PUEN_MASK);	/* URXD1 */
	atmel_pio4_set_a_periph(AT91_PIO_PORTD, 3, 0);	/* UTXD1 */

	at91_periph_clk_enable(ATMEL_ID_UART1);
}

void board_debug_uart_init(void)
{
	board_uart1_hw_init();
}
#endif

// RWS: Added
#ifdef CONFIG_NAND_ATMEL
static void board_nand_hw_init(void)
{
        struct at91_smc *smc = (struct at91_smc *)ATMEL_BASE_SMC;

        at91_periph_clk_enable(ATMEL_ID_HSMC);

        /* Configure SMC CS3 for NAND */
        writel(AT91_SMC_SETUP_NWE(1) | AT91_SMC_SETUP_NCS_WR(1) |
               AT91_SMC_SETUP_NRD(1) | AT91_SMC_SETUP_NCS_RD(1),
               &smc->cs[3].setup);
        writel(AT91_SMC_PULSE_NWE(2) | AT91_SMC_PULSE_NCS_WR(3) |
               AT91_SMC_PULSE_NRD(2) | AT91_SMC_PULSE_NCS_RD(3),
               &smc->cs[3].pulse);
        writel(AT91_SMC_CYCLE_NWE(5) | AT91_SMC_CYCLE_NRD(5),
               &smc->cs[3].cycle);
        writel(AT91_SMC_TIMINGS_TCLR(2) | AT91_SMC_TIMINGS_TADL(7) |
               AT91_SMC_TIMINGS_TAR(2)  | AT91_SMC_TIMINGS_TRR(3)   |
               AT91_SMC_TIMINGS_TWB(7)  | AT91_SMC_TIMINGS_RBNSEL(2)|
               AT91_SMC_TIMINGS_NFSEL(1), &smc->cs[3].timings);
        writel(AT91_SMC_MODE_RM_NRD | AT91_SMC_MODE_WM_NWE |
               AT91_SMC_MODE_EXNW_DISABLE |
               AT91_SMC_MODE_DBW_8 |
               AT91_SMC_MODE_TDF_CYCLE(1),
               &smc->cs[3].mode);

        atmel_pio4_set_f_periph(AT91_PIO_PORTA,  0, ATMEL_PIO_DRVSTR_ME);       /* D0 */
        atmel_pio4_set_f_periph(AT91_PIO_PORTA,  1, ATMEL_PIO_DRVSTR_ME);       /* D1 */
        atmel_pio4_set_f_periph(AT91_PIO_PORTA,  2, ATMEL_PIO_DRVSTR_ME);       /* D2 */
        atmel_pio4_set_f_periph(AT91_PIO_PORTA,  3, ATMEL_PIO_DRVSTR_ME);       /* D3 */
        atmel_pio4_set_f_periph(AT91_PIO_PORTA,  4, ATMEL_PIO_DRVSTR_ME);       /* D4 */
        atmel_pio4_set_f_periph(AT91_PIO_PORTA,  5, ATMEL_PIO_DRVSTR_ME);       /* D5 */
        atmel_pio4_set_f_periph(AT91_PIO_PORTA,  6, ATMEL_PIO_DRVSTR_ME);       /* D6 */
        atmel_pio4_set_f_periph(AT91_PIO_PORTA,  7, ATMEL_PIO_DRVSTR_ME);       /* D7 */
        atmel_pio4_set_f_periph(AT91_PIO_PORTA,12, 0);  /* RE */
        atmel_pio4_set_f_periph(AT91_PIO_PORTA,  8, 0); /* WE */
        atmel_pio4_set_f_periph(AT91_PIO_PORTA,  9, ATMEL_PIO_PUEN_MASK);       /* NCS */
        atmel_pio4_set_f_periph(AT91_PIO_PORTA,21, ATMEL_PIO_PUEN_MASK);        /* RDY */
        atmel_pio4_set_f_periph(AT91_PIO_PORTA,10, ATMEL_PIO_PUEN_MASK);        /* ALE */
        atmel_pio4_set_f_periph(AT91_PIO_PORTA,11, ATMEL_PIO_PUEN_MASK);        /* CLE */
}
#endif


#ifdef CONFIG_BOARD_EARLY_INIT_F
int board_early_init_f(void)
{
#ifdef CONFIG_DEBUG_UART
	debug_uart_init();
#endif

	return 0;
}
#endif

int board_init(void)
{
	/* address of boot parameters */
	gd->bd->bi_boot_params = CONFIG_SYS_SDRAM_BASE + 0x100;

#ifdef CONFIG_NAND_ATMEL
	board_nand_hw_init();
#endif

#ifdef CONFIG_CMD_USB
	board_usb_hw_init();
#endif

	board_spi_init();

	return 0;
}

int dram_init(void)
{
	gd->ram_size = get_ram_size((void *)CONFIG_SYS_SDRAM_BASE,
				    CONFIG_SYS_SDRAM_SIZE);
	return 0;
}

#ifdef CONFIG_MISC_INIT_R
int misc_init_r(void)
{
	return 0;
}
#endif

/* SPL */
#ifdef CONFIG_SPL_BUILD
void spl_board_init(void)
{
}

static void ddrc_conf(struct atmel_mpddrc_config *ddrc)
{
	ddrc->md = (ATMEL_MPDDRC_MD_DBW_32_BITS | ATMEL_MPDDRC_MD_DDR3_SDRAM);

	ddrc->cr = (ATMEL_MPDDRC_CR_NC_COL_10 |
		    ATMEL_MPDDRC_CR_NR_ROW_14 |
		    ATMEL_MPDDRC_CR_CAS_DDR_CAS5 |
		    ATMEL_MPDDRC_CR_DIC_DS |
		    ATMEL_MPDDRC_CR_DIS_DLL |
		    ATMEL_MPDDRC_CR_NB_8BANKS |
		    ATMEL_MPDDRC_CR_DECOD_INTERLEAVED |
		    ATMEL_MPDDRC_CR_UNAL_SUPPORTED);

	ddrc->rtr = 0x511;

	ddrc->tpr0 = (6 << ATMEL_MPDDRC_TPR0_TRAS_OFFSET |
		      3 << ATMEL_MPDDRC_TPR0_TRCD_OFFSET |
		      4 << ATMEL_MPDDRC_TPR0_TWR_OFFSET |
		      9 << ATMEL_MPDDRC_TPR0_TRC_OFFSET |
		      3 << ATMEL_MPDDRC_TPR0_TRP_OFFSET |
		      4 << ATMEL_MPDDRC_TPR0_TRRD_OFFSET |
		      4 << ATMEL_MPDDRC_TPR0_TWTR_OFFSET |
		      4 << ATMEL_MPDDRC_TPR0_TMRD_OFFSET);

	ddrc->tpr1 = (27 << ATMEL_MPDDRC_TPR1_TRFC_OFFSET |
		      29 << ATMEL_MPDDRC_TPR1_TXSNR_OFFSET |
		      0 << ATMEL_MPDDRC_TPR1_TXSRD_OFFSET |
		      3 << ATMEL_MPDDRC_TPR1_TXP_OFFSET);

	ddrc->tpr2 = (0 << ATMEL_MPDDRC_TPR2_TXARD_OFFSET |
		      0 << ATMEL_MPDDRC_TPR2_TXARDS_OFFSET |
		      0 << ATMEL_MPDDRC_TPR2_TRPA_OFFSET |
		      4 << ATMEL_MPDDRC_TPR2_TRTP_OFFSET |
		      7 << ATMEL_MPDDRC_TPR2_TFAW_OFFSET);
}

void mem_init(void)
{
	struct at91_pmc *pmc = (struct at91_pmc *)ATMEL_BASE_PMC;
	struct atmel_mpddr *mpddrc = (struct atmel_mpddr *)ATMEL_BASE_MPDDRC;
	struct atmel_mpddrc_config ddrc_config;
	u32 reg;

	ddrc_conf(&ddrc_config);

	at91_periph_clk_enable(ATMEL_ID_MPDDRC);
	writel(AT91_PMC_DDR, &pmc->scer);

	reg = readl(&mpddrc->io_calibr);
	reg &= ~ATMEL_MPDDRC_IO_CALIBR_RDIV;
	reg |= ATMEL_MPDDRC_IO_CALIBR_DDR3_RZQ_55;
	reg &= ~ATMEL_MPDDRC_IO_CALIBR_TZQIO;
	reg |= ATMEL_MPDDRC_IO_CALIBR_TZQIO_(100);
	writel(reg, &mpddrc->io_calibr);

	writel(ATMEL_MPDDRC_RD_DATA_PATH_SHIFT_TWO_CYCLE,
	       &mpddrc->rd_data_path);

	ddr3_init(ATMEL_BASE_MPDDRC, ATMEL_BASE_DDRCS, &ddrc_config);

	writel(0x3, &mpddrc->cal_mr4);
	writel(64, &mpddrc->tim_cal);
}

void at91_pmc_init(void)
{
	struct at91_pmc *pmc = (struct at91_pmc *)ATMEL_BASE_PMC;
	u32 tmp;

	/*
	 * while coming from the ROM code, we run on PLLA @ 492 MHz / 164 MHz
	 * so we need to slow down and configure MCKR accordingly.
	 * This is why we have a special flavor of the switching function.
	 */
	tmp = AT91_PMC_MCKR_PLLADIV_2 | \
	      AT91_PMC_MCKR_MDIV_3 | \
	      AT91_PMC_MCKR_CSS_MAIN;
	at91_mck_init_down(tmp);

	tmp = AT91_PMC_PLLAR_29 |
	      AT91_PMC_PLLXR_PLLCOUNT(0x3f) |
	      AT91_PMC_PLLXR_MUL(82) |
	      AT91_PMC_PLLXR_DIV(1);
	at91_plla_init(tmp);

	writel(0x0 << 8, &pmc->pllicpr);

	tmp = AT91_PMC_MCKR_H32MXDIV |
	      AT91_PMC_MCKR_PLLADIV_2 |
	      AT91_PMC_MCKR_MDIV_3 |
	      AT91_PMC_MCKR_CSS_PLLA;
	at91_mck_init(tmp);
}
#endif
