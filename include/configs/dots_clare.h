#ifndef __CONFIG_H
#define __CONFIG_H

#include "at91-sama5_common.h"

/* SDRAM */
#define CONFIG_NR_DRAM_BANKS		1
#define CONFIG_SYS_SDRAM_BASE		ATMEL_BASE_DDRCS
#define CONFIG_SYS_SDRAM_SIZE		0x10000000

#define CONFIG_SYS_INIT_SP_ADDR \
	(CONFIG_SYS_SDRAM_BASE + 16 * 1024 - GENERATED_GBL_DATA_SIZE)

#define CONFIG_SYS_LOAD_ADDR		0x22000000 /* load address */

#ifdef CONFIG_CMD_NAND
#define CONFIG_NAND_ATMEL
#define CONFIG_SYS_MAX_NAND_DEVICE	1
#define CONFIG_SYS_NAND_BASE		ATMEL_BASE_CS3

/* our ALE is AD21 */
#define CONFIG_SYS_NAND_MASK_ALE	(1 << 21)
/* our CLE is AD22 */
#define CONFIG_SYS_NAND_MASK_CLE	(1 << 22)
#define CONFIG_SYS_NAND_ONFI_DETECTION
/* PMECC & PMERRLOC */
#define CONFIG_ATMEL_NAND_HWECC
#define CONFIG_ATMEL_NAND_HW_PMECC

#define CONFIG_SYS_BOOTM_LEN			0x4000000

/* No environment */
#undef CONFIG_ENV_IS_IN_NAND
#define CONFIG_ENV_IS_NOWHERE

#undef CONFIG_BOOTCOMMAND
#define CONFIG_BOOTCOMMAND "ubi part nand0,2; ubifsmount ubi:dots; ubifsload 0x20000000 /boot/linux.itb; ubifsumount; source 20000000:script@1; \
setenv root_verity root,,,ro, 0 $verity_sectors verity 1 /dev/ubiblock0_0 /dev/ubiblock0_0 $verity_data_block_sz $verity_hash_block_sz $verity_data_blocks $verity_hash_start sha256 $verity_root_hash $verity_salt; \
setenv bootargs \"$bootargs dm-mod.create=\\\\\"$root_verity\\\\\" root=/dev/dm-0\"; \
bootm 0x20000000"

#undef CONFIG_BOOTARGS
#define CONFIG_BOOTARGS	"console=ttyS0,115200 earlyprintk " MTDPARTS_DEFAULT " spidev.bufsiz=8192 rootfstype=squashfs ubi.mtd=2 ubi.block=0,squashfs"

#endif /* #ifdef CONFIG_CMD_NAND */

/* For accessing UBIFS */
#define CONFIG_CMD_UBIFS
#define CONFIG_LZO
#define CONFIG_RBTREE
#define CONFIG_MTD_DEVICE
#define CONFIG_CMD_MTDPARTS
#define CONFIG_MTD_PARTITIONS
#define MTDIDS_DEFAULT		"nand0=atmel_nand"
#define MTDPARTS_DEFAULT	"mtdparts=atmel_nand:128k(bootstrap),512k(uboot),-(ubi_device)"

#endif /* #ifndef __CONFIG_H */
